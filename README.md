Hi there,

![](https://raw.githubusercontent.com/era/era/master/hey.webp)

- 🌱  I’m currently learning Rust, OS development and Machine Learning
- 👨🏽‍My [personal blog](https://news.elias.sh)
- &#9935; Checkout my [personal wiki!](https://wiki.anarchist.work) with all my personal notes about different subjects
- 👨🏽‍💻 I write about workplace and tech at https://www.masterless.io
- 📫 How to reach me: brtechie@protonmail.com
